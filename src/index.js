const fs = require('fs');
const ipcRender = require('electron').ipcRenderer;
let close_button;
let files_list;

document.addEventListener("DOMContentLoaded", () => {
	files_list = document.getElementById("files_list");
	close_button = document.getElementById("closeButton");
	close_button.addEventListener('click',(sender, event)=>{
		ipcRender.send('exit-app');
	});
	loadFilesFromCurrentDirectory();
});

function loadFilesFromCurrentDirectory() {
	fs.readdir(".", (err, files) => {
		files.forEach(file => {
			let li = document.createElement("LI");
			li.appendChild(document.createTextNode(file));
			files_list.appendChild(li);
		});
	});
}