const datagram = require('dgram');
const HostsList = require('./HostsList').HostsList;

function DiscoveryReceiver(bindingPort) {
    this.port = bindingPort;
    this.server = datagram.createSocket('udp4');
    this.listOfHosts = new HostsList();
}

DiscoveryReceiver.prototype.startUDP = function () {
    this.server.on('listening', () => {
        console.log("server is listening on ", this.server.address().address)
    });

    this.server.on('message', (msg, rinfo) => {
        this.listOfHosts.addNewHost(rinfo);
    });

    this.server.on('error', (err) => {
        this.server.close();
    });

    this.server.bind(this.port);
};

DiscoveryReceiver.prototype.stopUDP = function () {
    this.server.close();
};

module.exports.DiscoveryReceiver =  DiscoveryReceiver;
