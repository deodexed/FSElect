const EventEmitter = require('events');

class HostsList extends EventEmitter {

    constructor() {
        super();
        this.listOfHosts = [];

        this.getHost = function(index){
            return this.listOfHosts[index];
        };
    }

    addNewHost(host){
        let isFound = false;
        this.listOfHosts.forEach((value) =>{
            if(value.address === host.address){
                isFound = true;
            }
        });
        if(!isFound){
            this.listOfHosts.push(host);
            this.emit('newhost', host);
        } 
    }



}

module.exports.HostsList = HostsList;