const datagram = require('dgram');

module.exports.DiscoverySender = DiscoverySender;

function DiscoverySender() {

    this.sendingInformation = {
        destinationPort: 5666,
        destinationAddress: "255.255.255.255"
    };
    this.socket = datagram.createSocket({
        type: 'udp4',
        reuseAddr: true
    });
    this.socket.bind(5667, ()=>{
        this.socket.setBroadcast(true);
    });

    this.interval = null;

}

DiscoverySender.prototype.startUDP = function () {
    this.interval = setInterval(() => {
        this.socket.send("filesender", this.sendingInformation.destinationPort, this.sendingInformation.destinationAddress, (error => {
            if (error) {
                this.socket.close();
                clearInterval(this);
            } else {

            }
        }));
    }, 5000);
};


DiscoverySender.prototype.stopUDP = function () {
    clearInterval(this.interval);
    this.socket.close();
};

