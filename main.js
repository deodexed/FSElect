const {app, BrowserWindow} = require('electron');
const path = require('path');
const ipc = require('electron').ipcMain;
const ip = require('ip');
const hostname = require('os').hostname();
const DiscoveryReceiver = require('./src/DiscoveryReceiver').DiscoveryReceiver;
const DiscoverySender = require('./src/DiscoverySender').DiscoverySender;



let discoveryServer = new DiscoveryReceiver(5666);
let discoveryClient = new DiscoverySender();
let discoveredHosts = discoveryServer.listOfHosts;

let mainWindow = null;

let dummyHost = {
    address: "TEST ADDRESS"
};
let dummyHost2 = {
    address: "TEST ADDRESS2"
};
let dummyHost3 = {
    address: "TEST ADDRESS2"
};

function createWindow() {

    mainWindow = new BrowserWindow({width: 1400, height: 600, frame: false});
    mainWindow.loadFile("src/index.html");
    mainWindow.on("closed", onClosed);
    
    mainWindow.webContents.on("did-finish-load", (event, arg) => {
        startUDPService();
        discoveredHosts.on('newhost', (value)=>{
            console.log(value);
        });

        ipc.on('exit-app', ()=>{
            mainWindow.close();
        });
    });
   


}

function startUDPService() {
    console.log("starting UDP");
    discoveryServer.startUDP();
    discoveryClient.startUDP();
}

function stopUDPService() {
    console.log('stopping UDP');
    discoveryServer.stopUDP();
    discoveryClient.stopUDP();
}

function onClosed() {
    stopUDPService()
    mainWindow = null;
    app.quit();
}

app.on("ready", createWindow);//entry point

